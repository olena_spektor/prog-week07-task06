﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk07_task06
{
    class Program
    {
       /* Methods can hold more logic than a single line - you can also create variables in the methods

Create a method of type string that takes a single parameter of type int called `number` 
Inside the parameter create a variable with the name output of type string and give it an empty value `string.Empty()` 
Next create an if statement that checks if the value is even or not by using this condition
 `number % 2 == 0`  


Inside the true block of the `if statement` set the value of output to “This number is even”
Inside the false block of the `if statement` set the value of output to “This number is false”

At the end set the return output

Call the method 5 times with different numbers as the parameter */

        static void Main(string[] args)
        {
            string output = string.Empty;

            for (int i = 1; i <= 5; i++)
            {
                Console.Write("Type you number in here: ");
                int number = int.Parse(Console.ReadLine());
            
                Console.WriteLine(EvenOrOdd(number, output));                
            }           
        }

        static string EvenOrOdd(int number, string output)
        {                 
            if (number % 2 == 0)
            {
                output = "This number is even\n";                                
            }
            else
            {
                output = "This number is odd\n";
            }

            return output;                   
        }
    }
}